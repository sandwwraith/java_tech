/**
 * Contains simple UDP Client and Server to
 * show capabilities of network interaction.
 *
 * @author Leonid Startsev
 * sandwwraith@gmail.com
 * ITMO University, 2016
 **/
package ru.ifmo.ctddev.startsev.helloudp;