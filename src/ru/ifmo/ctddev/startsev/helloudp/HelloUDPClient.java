package ru.ifmo.ctddev.startsev.helloudp;

import info.kgeorgiy.java.advanced.hello.HelloClient;
import info.kgeorgiy.java.advanced.hello.Util;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Simple UDP Client.
 * Sends request to the server in form <tt>[prefix][threadNo]_[requestNo]</tt>.
 * Waits for response in form <tt>Hello, [prefix][threadNo]_[requestNo]</tt>.
 *
 * @author Leonid Startsev
 *         sandwwraith@gmail.com
 *         ITMO University, 2016
 **/
public class HelloUDPClient implements HelloClient {

    private static int parseArg(String arg, String name) {
        try {
            return Integer.parseInt(arg);
        } catch (NumberFormatException e) {
            System.err.println("Incorrect " + name);
            System.exit(-1);
        }
        return 0;
    }

    /**
     * Entry point for console program
     *
     * @param args Server address, Port, Request prefix, Number of threads, Number of requests per thread.
     */
    public static void main(String[] args) {
        if (args == null || args.length < 5) {
            throw new IllegalArgumentException("Not enough cmd args!");
        }
        String address = args[0];
        int port = parseArg(args[1], "port");
        String prefix = args[2];
        int threads = parseArg(args[3], "threads");
        int reqs = parseArg(args[4], "requests");

        new HelloUDPClient().start(address, port, prefix, reqs, threads);
    }

    /**
     * Starts the client and awaits of delivering all requests.
     *
     * @param host     Address of the server
     * @param port     Port
     * @param prefix   Requests prefix
     * @param requests Number of requests per thread
     * @param threads  Number of parallel threads
     */
    @Override
    public void start(String host, int port, String prefix, int requests, int threads) {
        try {
            ClientRunner runner = new ClientRunner(threads, prefix, host, port, requests);
            runner.run();
        } catch (InterruptedException e) {
            System.err.println("Interrupted");
        } catch (ExecutionException e) {
            System.err.println(e.getMessage());
        } catch (UnknownHostException e) {
            System.err.println("Unknown host");
        }
    }

    private static class ClientRunner {

        private final int threads;
        private final List<Callable<Void>> list = new ArrayList<>();

        ClientRunner(int threads, final String prefix, final String strAddress, final int port, final int requests) throws UnknownHostException {
            this.threads = threads;
            InetAddress address = InetAddress.getByName(strAddress);

            for (int i = 0; i < threads; ++i) {
                final int ii = i;
                list.add(() -> {
                    try (DatagramSocket sock = new DatagramSocket()) {
                        sock.setSoTimeout(500);
                        byte[] buf = new byte[sock.getReceiveBufferSize()];
                        DatagramPacket response = new DatagramPacket(buf, buf.length);
                        DatagramPacket request = new DatagramPacket(new byte[1], 1, address, port);
                        for (int j = 0; j < requests; ++j) {
                            setData(prefix, ii, j, request);
                            boolean handled = false;
                            String responseStr = expected(prefix, ii, j);
                            while (!handled) {
                                sock.send(request);

                                try {
                                    sock.receive(response);
                                    String s = new String(response.getData(), 0, response.getLength(), Util.CHARSET);
                                    if (s.equals(responseStr)) {
                                        System.out.println(s);
                                        handled = true;
                                    }
                                } catch (SocketTimeoutException e) {
                                    //not handled
                                }
                            }
                        }

                    } catch (IOException e) {
                        System.err.println(e.getMessage());
                    }
                    return null;
                });
            }
        }



        private void setData(String prefix, int thread, int pos, DatagramPacket packet) {
            String message = String.format("%s%d_%d", prefix, thread, pos);
            byte[] data = message.getBytes(Util.CHARSET);
            packet.setData(data, 0, data.length);
        }

        private String expected(String prefix, int thread, int pos) {
            return String.format("Hello, %s%d_%d", prefix, thread, pos);
        }

        public void run() throws InterruptedException, ExecutionException {
            final ExecutorService pool = Executors.newFixedThreadPool(threads);
            List<Future<Void>> a = pool.invokeAll(list);
            for (Future<Void> f : a) {
                f.get();
            }
            pool.shutdownNow();
        }
    }
}
