package ru.ifmo.ctddev.startsev.helloudp;

import info.kgeorgiy.java.advanced.hello.HelloServer;
import info.kgeorgiy.java.advanced.hello.Util;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Simple UDP Server.
 * Responses <tt>Hello, [request]</tt> for <tt>[request]</tt>
 *
 * @author Leonid Startsev
 *         sandwwraith@gmail.com
 *         ITMO University, 2016
 **/
public class HelloUDPServer implements HelloServer {

    private ServerRunner runner;

    /**
     * Entry point for console program
     *
     * @param args Port for server, Number of threads
     */
    public static void main(String[] args) {
        if (args == null || args.length < 2) {
            throw new IllegalArgumentException("Not enough cmd args!");
        }
        int port = Integer.parseInt(args[0]);
        int threads = Integer.parseInt(args[1]);

        try (ServerRunner runner = new ServerRunner(port, threads)) {
            System.out.println(String.format("Server running on %d port, hit enter to stop", port));
            //noinspection ResultOfMethodCallIgnored
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Starts the server on the provided <code>port</code>.
     *
     * @param port    Port to listen for
     * @param threads Number of worker threads
     */
    @Override
    public void start(int port, int threads) {
        try {
            runner = new ServerRunner(port, threads);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Closes the server and shut down all threads
     */
    @Override
    public void close() {
        runner.close();
    }

    private static class ServerRunner implements AutoCloseable {
        private final ExecutorService pool;
        private final DatagramSocket socket;

        ServerRunner(int port, int threads) throws SocketException {
            socket = new DatagramSocket(port);
            pool = Executors.newFixedThreadPool(threads);
            final int bufSize = socket.getReceiveBufferSize();
            Runnable task = () -> {
                try {
                    byte[] buf = new byte[bufSize];
                    DatagramPacket p = new DatagramPacket(buf, buf.length);

                    while (!Thread.interrupted()) {
                        socket.receive(p);
                        String response = "Hello, " + new String(buf, 0, p.getLength(), Util.CHARSET);
                        byte[] sendData = response.getBytes(Util.CHARSET);
                        p.setData(sendData, 0, sendData.length);
                        socket.send(p);
                        p.setData(buf, 0, buf.length);
                    }
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }

            };
            for (int i = 0; i < threads; ++i) {
                pool.submit(task);
            }
        }

        @Override
        public void close() {
            pool.shutdownNow();
            socket.close();
        }
    }
}
