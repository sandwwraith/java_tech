package ru.ifmo.ctddev.startsev.concurrent;

import info.kgeorgiy.java.advanced.concurrent.ListIP;
import info.kgeorgiy.java.advanced.mapper.ParallelMapper;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Provides methods for performing some
 * parallel operations on {@link List}.
 * <p>
 * Each method has a parameter describing how much threads to use.
 * Provided <code>List</code> will be split to chunks and every thread will
 * parallel perform operation on independent chunks.
 *
 * @author Created by sandwwraith(@gmail.com)
 *         ITMO University, 2016
 **/
public class IterativeParallelism implements ListIP {
    private ParallelMapper mapper = null;

    /**
     * Default constructor. Does nothing.
     */
    public IterativeParallelism() {
    }

    /**
     * If use this constructor, all methods will use {@link ParallelMapper} instead
     * of creating its own threads.
     * <p>
     * Parameter "numberOfThreads" in methods will be ignored.
     *
     * @param mapper Mapper to use
     */
    public IterativeParallelism(ParallelMapper mapper) {
        this.mapper = mapper;
    }

    /**
     * Concatenates string representations of <code>list</code> elements.
     * There are no separator between strings.
     *
     * @param i    Number of threads
     * @param list Source list
     * @return String with all elements of <code>list</code>
     * @throws InterruptedException If current thread has been interrupted
     */
    @Override
    public String join(int i, List<?> list) throws InterruptedException {
        Function<List<?>, String> joiner = objects ->
                objects.stream().map(Object::toString).collect(Collectors.joining());
        return parallelWork(i, list, joiner, joiner);
    }

    /**
     * Filters a list. Elements matching given <code>predicate</code> remains,
     * other elements are eliminated from result list.
     *
     * @param i         Number of threads
     * @param list      Source list
     * @param predicate Predicate to filter
     * @param <T>       Type of list elements
     * @return Filtered list
     * @throws InterruptedException If current thread has been interrupted.
     * @see Predicate
     * @see java.util.stream.Stream#filter(Predicate)
     */
    @Override
    public <T> List<T> filter(int i, List<? extends T> list, Predicate<? super T> predicate) throws InterruptedException {
        Function<List<? extends T>, List<T>> filt = ts -> ts.stream().filter(predicate).collect(Collectors.toList());
        Function<List<? extends List<T>>, List<T>> listJoiner = lists ->
                lists.stream().flatMap(Collection::stream).collect(Collectors.toList());
        return parallelWork(i, list, filt, listJoiner);
    }

    /**
     * Transforms each element of <code>list</code> using <code>function</code>.
     *
     * @param i        Number of threads
     * @param list     Source list
     * @param function Mapping function
     * @param <T>      Type of source elements
     * @param <U>      Type of result elements
     * @return Mapped list
     * @throws InterruptedException If current thread has been interrupted.
     * @see Function
     * @see java.util.stream.Stream#map(Function)
     */
    @Override
    public <T, U> List<U> map(int i, List<? extends T> list, Function<? super T, ? extends U> function) throws InterruptedException {
        Function<List<? extends T>, List<U>> mapper = ts -> ts.stream().map(function).collect(Collectors.toList());
        Function<List<? extends List<U>>, List<U>> listJoiner = lists ->
                lists.stream().flatMap(Collection::stream).collect(Collectors.toList());
        return parallelWork(i, list, mapper, listJoiner);
    }

    /**
     * Founds maximum of the given list.
     *
     * @param i          Number of threads
     * @param list       Source list
     * @param comparator Comparator to compare elements
     * @param <T>        Elements type
     * @return Maximum in the list
     * @throws InterruptedException             If current thread has been interrupted.
     * @throws java.util.NoSuchElementException If given <code>list</code> is empty.
     * @see Comparator
     * @see java.util.stream.Stream#max(Comparator)
     */
    @Override
    public <T> T maximum(int i, List<? extends T> list, Comparator<? super T> comparator) throws InterruptedException {
        Function<List<? extends T>, T> max = chunk -> Collections.max(chunk, comparator);
        return parallelWork(i, list, max, max);
    }

    /**
     * Founds minimum of the given list.
     *
     * @param i          Number of threads
     * @param list       Source list
     * @param comparator Comparator to compare elements
     * @param <T>        Elements type
     * @return Minimum in the list
     * @throws InterruptedException             If current thread has been interrupted.
     * @throws java.util.NoSuchElementException If given <code>list</code> is empty.
     * @see #maximum(int, List, Comparator)
     */
    @Override
    public <T> T minimum(int i, List<? extends T> list, Comparator<? super T> comparator) throws InterruptedException {
        return maximum(i, list, comparator.reversed());
    }

    /**
     * Checks whether all elements in the list are matching
     * given <code>predicate</code>
     *
     * @param i         Number of threads
     * @param list      Source list
     * @param predicate Predicate to test elements
     * @param <T>       Elements type
     * @return True if all elements match predicate; false otherwise
     * @throws InterruptedException If current thread has been interrupted.
     * @see Predicate
     * @see java.util.stream.Stream#allMatch(Predicate)
     */
    @Override
    public <T> boolean all(int i, List<? extends T> list, Predicate<? super T> predicate) throws InterruptedException {
        Function<List<? extends T>, Boolean> al = chunk -> chunk.stream().allMatch(predicate);
        return parallelWork(i, list, al, chunk -> chunk.stream().allMatch(x -> x));
    }

    /**
     * Checks whether any element in the list matches
     * given <code>predicate</code>
     *
     * @param i         Number of threads
     * @param list      Source list
     * @param predicate Predicate to test elements
     * @param <T>       Elements type
     * @return True if there are at least one element that matches predicate; false otherwise
     * @throws InterruptedException If current thread has been interrupted.
     * @see #all(int, List, Predicate)
     */
    @Override
    public <T> boolean any(int i, List<? extends T> list, Predicate<? super T> predicate) throws InterruptedException {
        return !all(i, list, predicate.negate());
    }

    /**
     * Manages creating, running and joining threads which preforms parallel
     * operations on {@link List}.
     * <p>
     * Automatically creates tasks for threads using source list and functions.
     * Source list will be split to chunks; each thread will perform task on one chunk.
     * Results of the task will be joined together using <code>finalizer</code>.
     * Then launches all tasks and awaits their finishing.
     *
     * @param threads   Number of threads
     * @param data      Source list
     * @param onChunk   Function to perform on independent part of list
     * @param finalizer Function to collect all results from list's parts
     * @param <T>       Type of elements in list
     * @param <R>       Return type
     * @return Result of performing all tasks.
     * @throws InterruptedException If current thread has been interrupted.
     */
    private <T, R> R parallelWork(int threads, List<? extends T> data,
                                  final Function<List<? extends T>, R> onChunk,
                                  final Function<? super List<R>, R> finalizer) throws InterruptedException {
        final List<List<? extends T>> list = split(data, threads);

        if (mapper != null) {
            //Doing with mapper
            return finalizer.apply(mapper.map(onChunk, list));
        }

        final List<R> results = new ArrayList<>(list.size());
        final List<Thread> tasks = new ArrayList<>(list.size());
        for (int i = 0; i < list.size(); i++) {
            final int ic = i;
            results.add(null);
            tasks.add(new Thread(() -> {
                R res = onChunk.apply(list.get(ic));
                synchronized (results) {
                    results.set(ic, res);
                }
            }));
            tasks.get(ic).start();
        }
        for (Thread t : tasks) {
            t.join();
        }
        return finalizer.apply(results);
    }

    /**
     * Splits given list in <code>i</code> chunks.
     * Or, if <tt>i &gt; list.size()</tt>, splits in
     * <tt>list.size()</tt> chunks.
     *
     * @param list List to split
     * @param i    Number of chunks
     * @param <U>  Element type
     * @return List contains subLists of original list
     * @see List#subList(int, int)
     */
    private <U> List<List<? extends U>> split(List<? extends U> list, int i) {
        int itemsPerOne = (list.size() + i - 1) / i;
        List<List<? extends U>> res = new ArrayList<>();
        for (int j = 0; j < list.size(); j += itemsPerOne) {
            res.add(list.subList(j, Math.min(j + itemsPerOne, list.size())));
        }
        return res;
    }
}
