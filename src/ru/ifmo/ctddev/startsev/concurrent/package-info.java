/**
 * Contains classes for performing parallel operations
 *
 * @author Created by sandwwraith(@gmail.com)
 * ITMO University, 2016
 * @version 1.0
 **/
package ru.ifmo.ctddev.startsev.concurrent;