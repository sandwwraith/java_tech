package ru.ifmo.ctddev.startsev.concurrent;

import info.kgeorgiy.java.advanced.mapper.ParallelMapper;

import java.util.*;
import java.util.function.Function;

/**
 * Can compute function on {@link List} parallel
 *
 * @author Created by sandwwraith(@gmail.com)
 *         ITMO University, 2016
 **/
public class ParallelMapperImpl implements ParallelMapper {

    private final Queue<Runnable> queue = new LinkedList<>();
    private final List<Thread> workers;

    /**
     * Creates new instance of mapper with <code>threads</code> number of worker threads
     *
     * @param threads Number of threads to create
     */
    public ParallelMapperImpl(int threads) {
        workers = new ArrayList<>(threads);
        Runnable runner = new Runnable() {
            @Override
            public void run() {
                try {
                    Runnable t;
                    while (!Thread.interrupted()) {
                        synchronized (queue) {
                            while (queue.isEmpty()) {
                                queue.wait();
                            }
                            t = queue.remove();
                        }
                        t.run();
                    }
                } catch (InterruptedException e) {
                    //Interrupted
                } finally {
                    Thread.currentThread().interrupt();
                }
            }
        };
        for (int i = 0; i < threads; ++i) {
            workers.add(new Thread(runner));
            workers.get(i).start();
        }
    }

    /**
     * Computes function on each element of list parallel.
     *
     * @param f    Function to compute
     * @param args List of elements to apply on
     * @param <T>  Source elements type
     * @param <R>  Type of elements of result list
     * @return Mapped list
     * @throws InterruptedException If current thread has been interrupted during work
     */
    public <T, R> List<R> map(Function<? super T, ? extends R> f, List<? extends T> args) throws InterruptedException {
        final Waiter<R> waiter = new Waiter<>(args.size());
        synchronized (queue) {
            for (int i = 0; i < args.size(); ++i) {
                final int ic = i;
                queue.add(() -> waiter.setResult(ic, f.apply(args.get(ic))));
                queue.notify();
            }
        }
        return waiter.getResults();
    }

    /**
     * Closes all workers threads
     *
     * @throws InterruptedException If current thread has been interrupted while waiting workers to finish
     */
    public void close() throws InterruptedException {
        workers.forEach(Thread::interrupt);
        for (Thread t : workers) {
            t.join();
        }
    }

    private class Waiter<R> {
        private final List<R> results;
        private int c = 0;

        Waiter(int size) {
            this.results = new ArrayList<>(Collections.nCopies(size, null));
            c = 0;
        }

        synchronized void setResult(int pos, R res) {
            results.set(pos, res);
            c++;
            if (c == results.size()) {
                this.notify();
            }
        }

        synchronized List<R> getResults() throws InterruptedException {
            while (c < results.size()) {
                this.wait();
            }
            return results;
        }
    }
}
