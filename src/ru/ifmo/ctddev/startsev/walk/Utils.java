package ru.ifmo.ctddev.startsev.walk;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by sandwwraith(@gmail.com)
 * ITMO University, 2016
 **/
public class Utils {

    public static final String EMPTY_HASH = "00000000000000000000000000000000";

    public static String MD5(Path filepath) throws NoSuchAlgorithmException, IOException {
        MessageDigest digest = MessageDigest.getInstance("MD5");
        try (InputStream stream = Files.newInputStream(filepath, StandardOpenOption.READ)) {
            byte[] buffer = new byte[1024];
            int c;
            while ((c = stream.read(buffer)) >= 0) {
                digest.update(buffer, 0, c);
            }
            return DatatypeConverter.printHexBinary(digest.digest());
        }

    }

    public static String MD5FormatIgnoreIOError(Path filepath) {
        String s;
        try {
            s = Utils.MD5(filepath);
        } catch (IOException e) {
            s = EMPTY_HASH;
        } catch (NoSuchAlgorithmException e) {
            System.err.println("MD5 is not supported on this machine");
            System.exit(-1);
            s = "UNREACHABLE CODE";
        }
        return formatHash(s, filepath);
    }

    public static String formatHash(String hash, Path file) {
        return hash + " " + file.toString();
    }
}
