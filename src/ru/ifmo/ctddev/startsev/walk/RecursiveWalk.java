package ru.ifmo.ctddev.startsev.walk;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created by sandwwraith(@gmail.com)
 * ITMO University, 2016
 **/
public class RecursiveWalk {

    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println("Not enough cmd arguments!");
            return;
        }

        try (BufferedReader reader = Files.newBufferedReader(Paths.get(args[0]), StandardCharsets.UTF_8)) {
            try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(args[1]), StandardCharsets.UTF_8)) {
                String filename;
                while ((filename = reader.readLine()) != null) {
                    try {
                        Files.walkFileTree(Paths.get(filename), new SimpleFileVisitor<Path>() {

                            @Override
                            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                                writer.write(Utils.MD5FormatIgnoreIOError(file));
                                writer.newLine();
                                return FileVisitResult.CONTINUE;
                            }


                            @Override
                            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                                writer.write(Utils.formatHash(Utils.EMPTY_HASH, file));
                                writer.newLine();
                                return FileVisitResult.CONTINUE;
                            }
                        });
                    } catch (SecurityException e) /*If the security manager denies access to the starting file.*/ {
                        writer.write(Utils.formatHash(Utils.EMPTY_HASH, Paths.get(filename)));
                        writer.newLine();
                    }

                }
            } catch (IOException e) {
                System.err.println("Error with output file: " + e.getMessage());
            }
        } catch (IOException e) {
            System.err.println("Error with input file: " + e.getMessage());
        }
    }

}
