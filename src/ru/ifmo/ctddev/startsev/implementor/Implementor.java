package ru.ifmo.ctddev.startsev.implementor;

import info.kgeorgiy.java.advanced.implementor.ImplerException;
import info.kgeorgiy.java.advanced.implementor.JarImpler;

import java.io.FilterWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Produces code implementing class or interface by its <tt>token</tt>.
 * <p>
 * <code>Implementor</code> collects all abstract methods from class, then generating code of each method
 * and writes it to file. Filename is equals to simple name of the class with suffix <tt>Impl</tt>.
 * Generated methods return default value, and generated constructors call <code>super()</code>.
 * For details of usage, see method {@link #implement(Class, Path)}
 *
 * @author Leonid Startsev
 *         <p>
 *         sandwwraith@gmail.com
 *         ITMO University, 2016
 * @version 1.0
 **/
public class Implementor implements JarImpler {

    /**
     * Default indentation in code.
     */
    private static final String INDENTATION = "    "; //4 spaces
    /**
     * Internal string buffer, where method's elements are appended during the generation.
     * Resets after writing each method to file.
     *
     * @see StringBuilder
     */
    private StringBuilder builder = new StringBuilder();
    /**
     * Type token that we are currently implementing.
     */
    private Class<?> clazz;

    /**
     * Default constructor. Does nothing.
     */
    public Implementor() {

    }

    /**
     * Generates the string representing the default value of class with given <code>token</code>.
     * <p>
     * It is <tt>null</tt> for non-primitive types,
     * <tt>false</tt> for {@link Boolean boolean},
     * empty string for {@link Void void}
     * and <tt>0</tt> for other.
     *
     * @param token type token to get default value for.
     * @return {@link String} with default value.
     */
    public static String defaultValue(Class<?> token) {
        if (!token.isPrimitive()) {
            return "null";
        } else if (token.equals(void.class)) {
            return "";
        } else if (token.equals(boolean.class)) {
            return "false";
        } else {
            return "0";
        }
    }

    /**
     * Builds a path, where implementation of <code>clazz</code> should be placed.
     * Implementation file has suffix <tt>Impl.java</tt> and
     * is placed in <tt>$root/&lt;package-dirs&gt;/</tt> folder.
     * <p>
     * All directories are creating automatically.
     *
     * @param clazz Token to resolve implementation path for
     * @param root  Directory to be treated as packages root
     * @return Path to file with implementation
     * @throws IOException If creation of directories has failed
     */
    public static Path getImplementedFilename(Class<?> clazz, Path root) throws IOException {
        if (clazz.getPackage() != null) {
            root = root.resolve(clazz.getPackage().getName().replace('.', '/') + "/");
            Files.createDirectories(root);
        }
        return root.resolve(clazz.getSimpleName() + "Impl.java");
    }

    /**
     * Produces code implementing class or interface specified by provided <tt>token</tt>.
     * <p>
     * Generated class full name should be same as full name of the type token with <tt>Impl</tt> suffix
     * added. Generated source code should be placed in the correct subdirectory of the specified
     * <tt>root</tt> directory and have correct file name. For example, the implementation of the
     * interface {@link List} should go to <tt>$root/java/util/ListImpl.java</tt>
     *
     * @param token type token to create implementation for.
     * @param root  root directory.
     * @throws ImplerException when implementation cannot be
     *                         generated.
     */
    @Override
    public void implement(Class<?> token, Path root) throws ImplerException {
        if (token == null || root == null) {
            throw new ImplerException("One or more arguments are null!");
        }

        this.clazz = token;

        if (Modifier.isFinal(clazz.getModifiers())) {
            throw new ImplerException("Class is final!");
        }

        try (Writer writer = new UnicodeFilter(Files.newBufferedWriter(getImplementedFilename(this.clazz, root)))) {
            if (clazz.getPackage() != null) {
                writer.write("package " + clazz.getPackage().getName() + ";" + System.lineSeparator());

            }
            writer.write(classHeader());

            int contCnt = 0;
            Constructor<?>[] constructors = clazz.getDeclaredConstructors();
            for (Constructor<?> cons : constructors) {
                if (!Modifier.isPrivate(cons.getModifiers())) {
                    writer.write(buildConstructor(cons));
                    contCnt++;
                }
            }

            if (contCnt == 0 && constructors.length > 0) {
                throw new ImplerException("No accessible constructors!");
            }

            Set<Method> methods = collectAbstractMethods(clazz);
            for (Method m : methods) {
                writer.write(buildMethod(m));
            }
            writer.write("}");
        } catch (IOException e) {
            throw new ImplerException("I/O Error", e);
        }
    }

    /**
     * Appends to {@link #builder} string with textual representation of all modifiers
     * allowed by <code>type</code> and provided by <code>mod</code>, excluding <code>abstract</code>.
     *
     * @param mod  Modifiers to print
     * @param type Mask for allowed modifiers
     * @see Modifier#toString(int)
     */
    private void modifiers(int mod, int type) {
        builder.append(Modifier.toString(mod & ~Modifier.ABSTRACT & type)).append(" ");
    }

    /**
     * Appends to {@link #builder} string representing method parameters.
     * i.e. their type tokens with default argument names, separated by commas.
     * Parenthesis are not included.
     * <p>
     * E.g. <tt>java.lang.String arg0, int arg1</tt>
     *
     * @param parameters array of method parameters
     */
    private void buildParameters(Parameter[] parameters) {
        for (int i = 0; i < parameters.length; i++) {
            Parameter p = parameters[i];
            modifiers(p.getModifiers(), Modifier.parameterModifiers());
            builder.append(p.getType().getCanonicalName());
            builder.append(' ');
            builder.append(p.getName());
            if (i != parameters.length - 1) {
                builder.append(", ");
            }
        }
    }

    /**
     * Generates code with implementation of given method.
     * Code does not do anything by returning default value of method's return type.
     * <p>
     * This method resets {@link #builder} and uses it to construct the text.
     *
     * @param m method to generate implementation for
     * @return String with code of implementation
     */
    private String buildMethod(Method m) {
        builder.setLength(0);

        builder.append(INDENTATION);
        modifiers(m.getModifiers(), Modifier.methodModifiers());
        builder
                .append(m.getReturnType().getCanonicalName())
                .append(" ")
                .append(m.getName())
                .append("(");
        buildParameters(m.getParameters());
        builder
                .append(")")
                .append(" {")
                .append(System.lineSeparator())
                .append(INDENTATION).append(INDENTATION).append("return ")
                .append(defaultValue(m.getReturnType()))
                .append(";")
                .append(System.lineSeparator())
                .append(INDENTATION).append("}")
                .append(System.lineSeparator());

        builder.append(System.lineSeparator());

        return builder.toString();
    }

    /**
     * Generates code with implementation of given constructor.
     * Code does not do anything by calling <code>super()</code> with corresponding arguments.
     * <p>
     * This method resets {@link #builder} and uses it to construct the text.
     *
     * @param cons Constructor to generate implementation for
     * @return String with code of implementation
     */
    private String buildConstructor(Constructor<?> cons) {
        builder.setLength(0);

        builder.append(INDENTATION);
        modifiers(cons.getModifiers(), Modifier.constructorModifiers());
        builder.append(clazz.getSimpleName()).append("Impl");
        builder.append("(");
        buildParameters(cons.getParameters());
        builder.append(")");
        Class<?>[] exc = cons.getExceptionTypes();
        if (exc.length > 0) {
            builder.append(" throws ");
            for (int i = 0; i < exc.length; i++) {
                builder.append(exc[i].getCanonicalName());
                if (i != exc.length - 1) {
                    builder.append(", ");
                }
            }
        }
        builder
                .append(" {")
                .append(System.lineSeparator())
                .append(INDENTATION)
                .append(INDENTATION)
                .append("super(");
        Parameter[] parameters = cons.getParameters();
        for (int i = 0; i < parameters.length; i++) {
            builder.append(parameters[i].getName());
            if (i != parameters.length - 1) {
                builder.append(", ");
            }
        }

        builder
                .append(");")
                .append(System.lineSeparator())
                .append(INDENTATION)
                .append("}")
                .append(System.lineSeparator())
                .append(System.lineSeparator());

        return builder.toString();
    }

    /**
     * Collects all abstract methods of class represented by <code>clazz</code> type token.
     * <p>
     * Abstract methods are collected from class itself, all its interfaces and superclasses.
     * If method is not overridden in this class, but has implementation in one of superclasses,
     * it is not considered abstract.
     *
     * @param clazz token to collect methods from
     * @return set of all abstract methods
     */
    private Set<Method> collectAbstractMethods(Class<?> clazz) {

        Set<MethodBox> abstractMethods = new HashSet<>();

        if (clazz == null || !Modifier.isAbstract(clazz.getModifiers())) {
            return Collections.emptySet();
        }

        for (Method m : clazz.getMethods()) {
            if (Modifier.isAbstract(m.getModifiers())) {
                abstractMethods.add(new MethodBox(m));
            }
        }

        while (clazz != null) {
            if (!Modifier.isAbstract(clazz.getModifiers())) {
                break;
            }

            for (Method m : clazz.getDeclaredMethods()) {
                int mod = m.getModifiers();
                if (!Modifier.isPrivate(mod) && !Modifier.isPublic(mod)) {
                    abstractMethods.add(new MethodBox(m));
                }
            }
            clazz = clazz.getSuperclass();
        }

        abstractMethods.removeIf(method -> !Modifier.isAbstract(method.m.getModifiers()));

        return abstractMethods.stream().map(box -> box.m).collect(Collectors.toSet());
    }

    /**
     * Generates code with class signature.
     * <p>
     * Class signature here - string like <i>
     * public class ClassName implements SomeInterface
     * </i>
     * <p>
     * Opening curly bracket is <b>included</b>
     * <p>
     * This method resets {@link #builder} and uses it to construct the text.
     *
     * @return String with code of implementation
     */
    private String classHeader() {
        builder.setLength(0);

        modifiers(clazz.getModifiers(), Modifier.classModifiers());
        builder.append("class ");
        builder.append(clazz.getSimpleName());
        builder.append("Impl ");
        if (clazz.isInterface()) {
            builder.append("implements ");
        } else {
            builder.append("extends ");
        }
        builder.append(clazz.getCanonicalName());
        builder.append(" {");
        builder.append(System.lineSeparator());
        return builder.toString();
    }

    /**
     * Produces <tt>.jar</tt> file implementing class or interface specified by provided <tt>token</tt>.
     * <p>
     * Generated class full name should be same as full name of the type token with <tt>Impl</tt> suffix
     * added.
     *
     * @param aClass type token to create implementation for.
     * @param path   target <tt>.jar</tt> file.
     * @throws ImplerException when implementation cannot be generated.
     */
    @Override
    public void implementJar(Class<?> aClass, Path path) throws ImplerException {
        try {
            Path tmp = Paths.get(".");

            this.implement(aClass, tmp);
            Path code = Implementor.getImplementedFilename(aClass, tmp).normalize();
            JARCompiler.compile(tmp, code);
            //code.toFile().deleteOnExit();
            Path classfile = JARCompiler.getClassFilename(code);
            JARCompiler.writeJAR(path, classfile);
            classfile.toFile().deleteOnExit();
        } catch (IOException e) {
            throw new ImplerException("I/O error: " + e.getMessage());
        } catch (CompilationException e) {
            throw new ImplerException(e);
        }
    }

    /**
     * Filters non-ASCII characters in output stream and converts it to "\\uXXXX" sequences.
     * <p>
     * You should use {@link #write(String, int, int)} to filter.
     */
    private class UnicodeFilter extends FilterWriter {

        /**
         * Construct the Filter on provided Writer
         *
         * @param out writer to filter
         */
        protected UnicodeFilter(Writer out) {
            super(out);
        }

        /**
         * Replaces unicode characters in <code>string</code> to "\\uXXXX" sequences
         * <p>
         * {@inheritDoc}
         */
        @Override
        public void write(String string, int off, int len) throws IOException {
            StringBuilder b = new StringBuilder();
            for (char c : string.substring(off, off + len).toCharArray()) {
                if (c >= 128) {
                    b.append(String.format("\\u%04X", (int) c));
                } else {
                    b.append(c);
                }
            }
            super.write(b.toString(), 0, b.length());
        }
    }

    /**
     * A box class for comparing {@link Method methods} by their name and parameters types.
     * Also, it provides functionality to build a {@link HashSet} of it.
     */
    private class MethodBox {
        /**
         * Boxed {@link Method}
         */
        public final Method m;

        /**
         * Boxing constructor
         *
         * @param m underlying {@link Method}
         */
        private MethodBox(Method m) {
            this.m = m;
        }

        /**
         * Two methods are considering equals here, if they have have same names and parameters types.
         * {@inheritDoc}
         */
        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof MethodBox)) {
                return false;
            }
            Method m2 = ((MethodBox) obj).m;

            return m.getName().equals(m2.getName()) && Arrays.equals(m.getParameterTypes(), m2.getParameterTypes());
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int hashCode() {
            int hash = m.getName().hashCode();
            for (Parameter p : m.getParameters()) {
                hash ^= p.getType().hashCode();
            }
            return hash;
        }
    }

}
