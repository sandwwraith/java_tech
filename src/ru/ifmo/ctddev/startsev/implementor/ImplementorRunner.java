package ru.ifmo.ctddev.startsev.implementor;

import info.kgeorgiy.java.advanced.implementor.ImplerException;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Provides an {@link #main(String[]) entry point} for implementing, compiling and packing
 * into JAR classes and interfaces with the command line.
 *
 * @author Leonid Startsev
 *         <p>
 *         sandwwraith@gmail.com
 *         ITMO University, 2016
 * @version 1.0
 * @see Implementor
 **/
public class ImplementorRunner {

    /**
     * Only static methods of class should be used, so default constructor made private.
     * <p>
     * Does nothing.
     */
    private ImplementorRunner() {

    }

    /**
     * Entry point for program.
     * <p>
     * Usage:
     * <ul>
     * <li><b>&lt;class&gt;</b> - generate implementation of <i>class</i> to <i>classImpl.java</i> </li>
     * <li><b>-jar &lt;class&gt; &lt;jar_name&gt; </b> - generate implementation of &lt;class&gt;,
     * compile it, and pack into &lt;jar_name&gt; JAR archive</li>
     * </ul>
     *
     * @param args Command line arguments, as described above.
     */
    public static void main(String[] args) {

        if (args == null || args.length < 1) {
            System.err.println("Arguments are not valid");
            return;
        }

        boolean jarMode = false;
        String className;

        if (args[0].equals("-jar")) {
            if (args.length < 3) {
                System.err.println("Not enough args for jar usage");
                return;
            }

            jarMode = true;
            className = args[1];
        } else {
            className = args[0];
        }

        try {
            Class<?> cls = Class.forName(className);
            Implementor imp = new Implementor();
            Path root = Paths.get(".");

            if (!jarMode) {
                imp.implement(cls, root);
            } else {
                imp.implementJar(cls, root);
            }

        } catch (ClassNotFoundException e) {
            System.err.println("Class not found!");
        } catch (ImplerException e) {
            System.err.println("Cannot implement a class: " + e.getMessage());
        }
    }
}
