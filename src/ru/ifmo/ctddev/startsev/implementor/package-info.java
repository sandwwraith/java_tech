/**
 * Package contains classes which can implement, compile and pack to JAR
 * abstract classes and interfaces.
 *
 * @author Leonid Startsev
 *         <p>
 *         sandwwraith@gmail.com
 *         ITMO University, 2016
 * @version 1.0
 **/
package ru.ifmo.ctddev.startsev.implementor;