package ru.ifmo.ctddev.startsev.implementor;

/**
 * This exception is thrown when a compilation error occurs.
 *
 * @author Leonid Startsev
 *         <p>
 *         sandwwraith@gmail.com
 *         ITMO University, 2016
 * @version 1.0
 * @see JARCompiler#compile
 **/
public class CompilationException extends Exception {

    /**
     * Constructs exception with a message about error.
     *
     * @param message Error description
     */
    public CompilationException(String message) {
        super(message);
    }
}
