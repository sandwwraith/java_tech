package ru.ifmo.ctddev.startsev.arrayset;

import java.util.*;

/**
 * Created by sandwwraith(@gmail.com)
 * ITMO University, 2016
 **/


public class ArraySet<E> extends AbstractSet<E> implements NavigableSet<E> {

    private final List<E> data;
    private final Comparator<? super E> comp;

    public ArraySet() {
        this(Collections.emptyList(), null, true);
    }

    public ArraySet(Collection<? extends E> col) {
        this(col, null);
    }

    public ArraySet(Collection<? extends E> col, Comparator<? super E> comp) {
        data = new ArrayList<>();
        this.comp = comp;
        Set<E> s = new TreeSet<>(comp);
        s.addAll(col);
        data.addAll(s);
    }

    //Reference copy constructor. Boolean arg for distinguishing Collection constructor from List constructor
    private ArraySet(List<E> data, Comparator<? super E> comp, boolean refcpy) {
        this.data = data;
        this.comp = comp;
    }

    @Override
    public int size() {
        return data.size();
    }

    /**
     * Returns an iterator over the elements in this set, in ascending order.
     *
     * @return an iterator over the elements in this set, in ascending order
     */
    @Override
    public Iterator<E> iterator() {
        return Collections.unmodifiableList(data).iterator();
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>This implementation uses binary search to test if element presents or not
     *
     * @param o
     * @throws ClassCastException   {@inheritDoc}
     * @throws NullPointerException {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public boolean contains(Object o) {
        return Collections.binarySearch(data, o, (Comparator<Object>) comp) >= 0;
    }

    /**
     * Returns the comparator used to order the elements in this set,
     * or <tt>null</tt> if this set uses the {@linkplain Comparable
     * natural ordering} of its elements.
     *
     * @return the comparator used to order the elements in this set,
     * or <tt>null</tt> if this set uses the natural ordering
     * of its elements
     */
    @Override
    public Comparator<? super E> comparator() {
        return comp;
    }

    /**
     * Returns the first (lowest) element currently in this set.
     *
     * @return the first (lowest) element currently in this set
     * @throws NoSuchElementException if this set is empty
     */
    @Override
    public E first() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return data.get(0);
    }

    /**
     * Returns the last (highest) element currently in this set.
     *
     * @return the last (highest) element currently in this set
     * @throws NoSuchElementException if this set is empty
     */
    @Override
    public E last() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return data.get(size() - 1);
    }

    /**
     * Returns the greatest element in this set strictly less than the
     * given element, or {@code null} if there is no such element.
     *
     * @param e the value to match
     * @return the greatest element less than {@code e},
     * or {@code null} if there is no such element
     * @throws ClassCastException   if the specified element cannot be
     *                              compared with the elements currently in the set
     * @throws NullPointerException if the specified element is null
     *                              and this set does not permit null elements
     */
    @Override
    public E lower(E e) {
        int ind = lowerInd(e);
        return ind == -1 ? null : data.get(ind);
    }

    private int lowerInd(E e) {
        final int ind = Collections.binarySearch(data, e, comp);
        if (ind >= 0) {
            return ind - 1;
        }
        return -(ind + 1) - 1;
    }

    /**
     * Returns the greatest element in this set less than or equal to
     * the given element, or {@code null} if there is no such element.
     *
     * @param e the value to match
     * @return the greatest element less than or equal to {@code e},
     * or {@code null} if there is no such element
     * @throws ClassCastException   if the specified element cannot be
     *                              compared with the elements currently in the set
     * @throws NullPointerException if the specified element is null
     *                              and this set does not permit null elements
     */
    @Override
    public E floor(E e) {
        int ind = floorInd(e);
        return ind == -1 ? null : data.get(ind);
    }

    private int floorInd(E e) {
        int ind = Collections.binarySearch(data, e, comp);
        if (ind < 0) {
            ind = -(ind + 1);
            return ind - 1;
        } else {
            return ind;
        }
    }

    /**
     * Returns the least element in this set greater than or equal to
     * the given element, or {@code null} if there is no such element.
     *
     * @param e the value to match
     * @return the least element greater than or equal to {@code e},
     * or {@code null} if there is no such element
     * @throws ClassCastException   if the specified element cannot be
     *                              compared with the elements currently in the set
     * @throws NullPointerException if the specified element is null
     *                              and this set does not permit null elements
     */
    @Override
    public E ceiling(E e) {
        int ind = ceilingInd(e);
        return ind == data.size() ? null : data.get(ind);
    }

    private int ceilingInd(E e) {
        int ind = Collections.binarySearch(data, e, comp);
        if (ind < 0) {
            ind = -(ind + 1);
        }
        return ind;
    }

    /**
     * Returns the least element in this set strictly greater than the
     * given element, or {@code null} if there is no such element.
     *
     * @param e the value to match
     * @return the least element greater than {@code e},
     * or {@code null} if there is no such element
     * @throws ClassCastException   if the specified element cannot be
     *                              compared with the elements currently in the set
     * @throws NullPointerException if the specified element is null
     *                              and this set does not permit null elements
     */
    @Override
    public E higher(E e) {
        int ind = higherInd(e);
        return ind == data.size() ? null : data.get(ind);
    }

    private int higherInd(E e) {
        int ind = Collections.binarySearch(data, e, comp);
        if (ind >= 0) {
            return ind + 1;
        } else {
            ind = -(ind + 1);
            return ind;
        }
    }


    /**
     * Retrieves and removes the last (highest) element.
     * This set implementation is unmodifiable, so this method
     * always throws {@code UnsupportedOperationException}
     */
    @Override
    public E pollLast() {
        throw new UnsupportedOperationException("poll");
    }

    /**
     * Retrieves and removes the first (lowest) element,
     * This set implementation is unmodifiable, so this method
     * always throws {@code UnsupportedOperationException}
     */
    @Override
    public E pollFirst() {
        throw new UnsupportedOperationException("poll");
    }

    /**
     * Returns a view of the portion of this set whose elements range from
     * {@code fromElement} to {@code toElement}.  If {@code fromElement} and
     * {@code toElement} are equal, the returned set is empty unless {@code
     * fromInclusive} and {@code toInclusive} are both true.  The returned set
     * is backed by this set, so changes in the returned set are reflected in
     * this set, and vice-versa.  The returned set supports all optional set
     * operations that this set supports.
     * <p>
     * <p>The returned set will throw an {@code IllegalArgumentException}
     * on an attempt to insert an element outside its range.
     *
     * @param fromElement   low endpoint of the returned set
     * @param fromInclusive {@code true} if the low endpoint
     *                      is to be included in the returned view
     * @param toElement     high endpoint of the returned set
     * @param toInclusive   {@code true} if the high endpoint
     *                      is to be included in the returned view
     * @return a view of the portion of this set whose elements range from
     * {@code fromElement}, inclusive, to {@code toElement}, exclusive
     * @throws ClassCastException       if {@code fromElement} and
     *                                  {@code toElement} cannot be compared to one another using this
     *                                  set's comparator (or, if the set has no comparator, using
     *                                  natural ordering).  Implementations may, but are not required
     *                                  to, throw this exception if {@code fromElement} or
     *                                  {@code toElement} cannot be compared to elements currently in
     *                                  the set.
     * @throws NullPointerException     if {@code fromElement} or
     *                                  {@code toElement} is null and this set does
     *                                  not permit null elements
     * @throws IllegalArgumentException if {@code fromElement} is
     *                                  greater than {@code toElement}; or if this set itself
     *                                  has a restricted range, and {@code fromElement} or
     *                                  {@code toElement} lies outside the bounds of the range.
     */
    @Override
    public NavigableSet<E> subSet(E fromElement, boolean fromInclusive, E toElement, boolean toInclusive) {

        int lo = fromInclusive ? ceilingInd(fromElement) : higherInd(fromElement);
        int hi = toInclusive ? floorInd(toElement) : lowerInd(toElement);

        if (lo > hi) {
            //Check for equality from and to
            if (comp != null) {
                if (comp.compare(fromElement, toElement) == 0) {
                    return new ArraySet<>(new ArrayList<>(), comp, true);
                }
            } else {
                if (fromElement.equals(toElement)) {
                    return new ArraySet<>(new ArrayList<>(), null, true);
                }
            }
        }

        return new ArraySet<>(data.subList(lo, hi + 1), comp, true);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>Equivalent to {@code subSet(fromElement, true, toElement, false)}.
     *
     * @param fromElement
     * @param toElement
     * @throws ClassCastException       {@inheritDoc}
     * @throws NullPointerException     {@inheritDoc}
     * @throws IllegalArgumentException {@inheritDoc}
     */
    @Override
    public SortedSet<E> subSet(E fromElement, E toElement) {
        return subSet(fromElement, true, toElement, false);
    }

    /**
     * Returns a view of the portion of this set whose elements are less than
     * (or equal to, if {@code inclusive} is true) {@code toElement}.  The
     * returned set is backed by this set, so changes in the returned set are
     * reflected in this set, and vice-versa.  The returned set supports all
     * optional set operations that this set supports.
     * <p>
     * <p>The returned set will throw an {@code IllegalArgumentException}
     * on an attempt to insert an element outside its range.
     *
     * @param toElement high endpoint of the returned set
     * @param inclusive {@code true} if the high endpoint
     *                  is to be included in the returned view
     * @return a view of the portion of this set whose elements are less than
     * (or equal to, if {@code inclusive} is true) {@code toElement}
     * @throws ClassCastException       if {@code toElement} is not compatible
     *                                  with this set's comparator (or, if the set has no comparator,
     *                                  if {@code toElement} does not implement {@link Comparable}).
     *                                  Implementations may, but are not required to, throw this
     *                                  exception if {@code toElement} cannot be compared to elements
     *                                  currently in the set.
     * @throws NullPointerException     if {@code toElement} is null and
     *                                  this set does not permit null elements
     * @throws IllegalArgumentException if this set itself has a
     *                                  restricted range, and {@code toElement} lies outside the
     *                                  bounds of the range
     */
    @Override
    public NavigableSet<E> headSet(E toElement, boolean inclusive) {
        int hi = inclusive ? floorInd(toElement) : lowerInd(toElement);
        return new ArraySet<>(data.subList(0, hi + 1), comp, true);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>Equivalent to {@code headSet(toElement, false)}.
     *
     * @param toElement
     * @throws ClassCastException       {@inheritDoc}
     * @throws NullPointerException     {@inheritDoc}
     * @throws IllegalArgumentException {@inheritDoc}
     */
    @Override
    public SortedSet<E> headSet(E toElement) {
        return headSet(toElement, false);
    }

    /**
     * Returns a view of the portion of this set whose elements are greater
     * than (or equal to, if {@code inclusive} is true) {@code fromElement}.
     * The returned set is backed by this set, so changes in the returned set
     * are reflected in this set, and vice-versa.  The returned set supports
     * all optional set operations that this set supports.
     * <p>
     * <p>The returned set will throw an {@code IllegalArgumentException}
     * on an attempt to insert an element outside its range.
     *
     * @param fromElement low endpoint of the returned set
     * @param inclusive   {@code true} if the low endpoint
     *                    is to be included in the returned view
     * @return a view of the portion of this set whose elements are greater
     * than or equal to {@code fromElement}
     * @throws ClassCastException       if {@code fromElement} is not compatible
     *                                  with this set's comparator (or, if the set has no comparator,
     *                                  if {@code fromElement} does not implement {@link Comparable}).
     *                                  Implementations may, but are not required to, throw this
     *                                  exception if {@code fromElement} cannot be compared to elements
     *                                  currently in the set.
     * @throws NullPointerException     if {@code fromElement} is null
     *                                  and this set does not permit null elements
     * @throws IllegalArgumentException if this set itself has a
     *                                  restricted range, and {@code fromElement} lies outside the
     *                                  bounds of the range
     */
    @Override
    public NavigableSet<E> tailSet(E fromElement, boolean inclusive) {
        int lo = inclusive ? ceilingInd(fromElement) : higherInd(fromElement);
        return new ArraySet<>(data.subList(lo, data.size()), comp, true);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>Equivalent to {@code tailSet(fromElement, true)}.
     *
     * @param fromElement
     * @throws ClassCastException       {@inheritDoc}
     * @throws NullPointerException     {@inheritDoc}
     * @throws IllegalArgumentException {@inheritDoc}
     */
    @Override
    public SortedSet<E> tailSet(E fromElement) {
        return tailSet(fromElement, true);
    }

    /**
     * Returns a reverse order view of the elements contained in this set.
     * The descending set is backed by this set, so changes to the set are
     * reflected in the descending set, and vice-versa.  If either set is
     * modified while an iteration over either set is in progress (except
     * through the iterator's own {@code remove} operation), the results of
     * the iteration are undefined.
     * <p>
     * <p>The returned set has an ordering equivalent to
     * <tt>{@link Collections#reverseOrder(Comparator) Collections.reverseOrder}(comparator())</tt>.
     * The expression {@code s.descendingSet().descendingSet()} returns a
     * view of {@code s} essentially equivalent to {@code s}.
     *
     * @return a reverse order view of this set
     */
    @Override
    public NavigableSet<E> descendingSet() {
        return new ArraySet<>(new ReversedArrayList<>(data), Collections.reverseOrder(comp), true);
    }

    /**
     * Returns an iterator over the elements in this set, in descending order.
     * Equivalent in effect to {@code descendingSet().iterator()}.
     *
     * @return an iterator over the elements in this set, in descending order
     */
    @Override
    public Iterator<E> descendingIterator() {
        return new ReversedArrayList<>(data).iterator();
    }

    @Override
    public Spliterator<E> spliterator() {
        return Spliterators.spliterator(this, Spliterator.DISTINCT | Spliterator.IMMUTABLE | Spliterator.SIZED | Spliterator.SORTED);
    }

    private class ReversedArrayList<T> extends AbstractList<T> implements RandomAccess {

        private final List<T> substrate;
        private final boolean direct;

        ReversedArrayList(List<T> substrate) {
            if (substrate instanceof ReversedArrayList) {
                ReversedArrayList<T> t = (ReversedArrayList<T>) substrate;
                this.substrate = t.substrate;
                direct = !t.direct;
                return;
            }
            if (!(substrate instanceof RandomAccess)) {
                //Should always be random access
                throw new IllegalArgumentException("Substrate must be random access");
            }
            this.substrate = substrate;
            direct = false;
        }

        @Override
        public int size() {
            return substrate.size();
        }

        @Override
        public T get(int index) {
            return direct ? substrate.get(index) : substrate.get(size() - index - 1);
        }

    }
}
