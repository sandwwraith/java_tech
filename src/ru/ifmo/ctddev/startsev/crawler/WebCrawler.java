package ru.ifmo.ctddev.startsev.crawler;

import info.kgeorgiy.java.advanced.crawler.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.*;
import java.util.concurrent.*;

/**
 * Very simple parallel web crawler
 *
 * @author Created by sandwwraith(@gmail.com)
 *         ITMO University, 2016
 **/
public class WebCrawler implements Crawler {
    private final Downloader downloader;
    private final ExecutorService downloaderPool;
    private final ExecutorService extractorPool;
    private final HostManager manager;

    /**
     * Creates new instance of web crawler.
     *
     * @param downloader  {@link Downloader} to use
     * @param downloaders Amount of threads for downloading
     * @param extractors  Amount of threads to extracting
     * @param perHost     Maximum parallel downloads per one host
     */
    public WebCrawler(Downloader downloader, int downloaders, int extractors, int perHost) {
        downloaderPool = Executors.newFixedThreadPool(downloaders);
        extractorPool = Executors.newFixedThreadPool(extractors);
        manager = new HostManager(perHost);
        this.downloader = downloader;
    }

    private static int getArg(String[] args, int index, int defVal) {
        if (args.length >= index + 1) {
            return Integer.parseInt(args[index]);
        }
        return defVal;
    }

    /**
     * Class for launching crawler from the console, with {@link CachingDownloader} as downloader.
     * Depth is 2.
     * Default values for amount of threads is {@link Runtime#availableProcessors()}
     *
     * @param args url [downloads [extractors [perHost]]]
     */
    public static void main(String[] args) {
        if (args == null || args.length < 1) {
            System.err.println("Not enough cmd args!");
            System.exit(-1);
        }
        int proc = Runtime.getRuntime().availableProcessors();
        String url = args[0];
        int downloaders = getArg(args, 1, proc);
        int extractors = getArg(args, 2, proc);
        int perHost = getArg(args, 3, proc * 2);
        try (WebCrawler crawler = new WebCrawler(new CachingDownloader(), downloaders, extractors, perHost)) {
            crawler.download(url, 2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Crawl the given site in parallel
     *
     * @param url   Start address
     * @param depth Depth of crawl. 1 - only given url. 2 - urls on the start page, and so on...
     * @return {@link Result} with list of loaded urls and errors.
     */
    @Override
    public Result download(String url, int depth) {
        Phaser phaser = new Phaser(1);
        Set<String> result = ConcurrentHashMap.newKeySet();
        Map<String, IOException> errors = new ConcurrentHashMap<>();

        download(url, phaser, result, errors, depth);
        phaser.arriveAndAwaitAdvance();
        result.removeAll(errors.keySet());
        return new Result(new ArrayList<>(result), errors);
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void download(String url, final Phaser phaser, final Set<String> result, final Map<String, IOException> errors, int depth) {
        String host;
        try {
            host = URLUtils.getHost(url);
        } catch (MalformedURLException e) {
            errors.put(url, e);
            return;
        }
        if (depth > 0 && result.add(url)) {
            phaser.register();
            manager.acquire(host, () -> {
                try {
                    final Document doc = downloader.download(url);
                    if (depth == 1) {
                        return;
                    }
                    phaser.register();
                    extractorPool.submit(() -> {
                        try {
                            List<String> urls = doc.extractLinks();
                            urls.forEach(u -> download(u, phaser, result, errors, depth - 1));
                        } catch (IOException e) {
                            errors.put(url, e);
                        } finally {
                            phaser.arrive();
                        }
                    });
                } catch (IOException e) {
                    errors.put(url, e);
                } finally {
                    manager.release(host);
                    phaser.arrive();
                }
            });
        }
    }

    /**
     * Shutdowns all helper threads. Required to call.
     */
    @Override
    public void close() {
        downloaderPool.shutdownNow();
        extractorPool.shutdownNow();
    }

    private class HostManager {
        private final Map<String, Integer> load = new ConcurrentHashMap<>();
        private final Map<String, Queue<Runnable>> hostQueue = new ConcurrentHashMap<>();
        private final int maxLoad;

        private HostManager(int maxLoad) {
            this.maxLoad = maxLoad;
        }

        void acquire(String host, Runnable task) {
            load.putIfAbsent(host, 0);
            if (load.get(host) < maxLoad) {
                load.compute(host, (h, cur) -> cur + 1);
                downloaderPool.submit(task);
            } else {
                hostQueue.putIfAbsent(host, new ConcurrentLinkedQueue<>());
                hostQueue.get(host).add(task);
            }
        }

        void release(String host) {
            if (hostQueue.containsKey(host)) {
                final Queue<Runnable> q = hostQueue.get(host);
                synchronized (q) {
                    if (!q.isEmpty()) {
                        downloaderPool.submit(q.poll());
                    }
                    if (q.isEmpty()) {
                        hostQueue.remove(host);
                    }
                }
            } else {
                load.compute(host, (h, cur) -> cur - 1);
            }
        }
    }
}
