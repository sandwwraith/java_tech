/**
 * Package contains a simple Web Crawler.
 *
 * @author Created by sandwwraith(@gmail.com)
 * ITMO University, 2016
 **/
package ru.ifmo.ctddev.startsev.crawler;